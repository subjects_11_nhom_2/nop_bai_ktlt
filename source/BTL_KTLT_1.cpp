#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<string>

using namespace std;

struct TriedNode{
	TriedNode*A[26];
	bool endOfWord;
};
struct mangChuaTu{
	char Tu[30] = "";
	char viTri[500] = "";
};

// bien toan cuc
mangChuaTu array[1000];
int tongSoTu = 0;
char grid[100][100];

// tao trie
TriedNode*init(){
	TriedNode*p = (TriedNode*)malloc(sizeof(TriedNode));
	p -> endOfWord = false;
	for(int i = 0;i < 26;i ++){   
		p -> A[i] = NULL;
	}
}

void luuKetQua(int x, int y, char word[]);

// CHEN TU VAO CAY TRIE
void insert(TriedNode*root, char word[]){
	int length = strlen(word);
	for(int i = 0;i < length; i++){
		int local = word[i] - 'a';
		if(root -> A[local] == NULL){
			TriedNode*p = init();
			root -> A[local] = p;
		}
		root = root -> A[local];
	}
	root -> endOfWord = true;
}
// LAY DU LIEU TU FILE DICTIONARY
void layDuLieu(TriedNode*root){
	FILE* taptin = NULL;
	taptin = fopen("dictionary.txt","r");
	char chuoi[30] = "";
	if(taptin != NULL){
		while(fgets(chuoi,30,taptin) != NULL){
			chuoi[strlen(chuoi) - 1] = '\0';
			insert(root, chuoi);
		}
		fclose(taptin);
	}
	cout << endl;
}
// chuyen ki tu ve chu thuong
char get(char x){
	if(x >= 'a' && x <= 'z') return x;
	return char(x + 'a' - 'A');
}
// LAY DU LIEU VA IN FILE GRID.TXT
void docVaInFileGrid(int &sizeX, int &sizeY){
	FILE* taptin = NULL;
	char  a[100];
	cout << "Nhap link file gird vao: (theo dang A:/B/C.../gird.txt)" << endl;
	cin >> a;
	char *link = a;
	taptin = fopen(link, "r");
	while(taptin==NULL)
	{
		cout << "Khong lay duoc file Gird. De nghi nhap lai!! " << endl;
		cin >> a;
		char *link = a; 
		taptin = fopen(link, "r");
	}
	if(taptin != NULL){
		char layKiTu = fgetc(taptin);
		while(layKiTu != ' '){
			sizeX = sizeX * 10 + (int)layKiTu - 48;
			layKiTu = fgetc(taptin);
		}
		layKiTu = fgetc(taptin);
		while(layKiTu != '\n'){
			sizeY = sizeY * 10 + (int)layKiTu - 48;
			layKiTu = fgetc(taptin);
		}
		fgetc(taptin);
		printf("\nGRID : %d * %d\n\n", sizeX, sizeY);
		for(int i = 0;i < sizeX; i++){
			for(int j = 0;j < sizeY; j++){
				char s = fgetc(taptin) ;
				printf("%c ", s);
				grid[i][j] = get(s);
				fgetc(taptin);
			}
			printf("\n");
		}
		printf("\n\n");
		fclose(taptin);
	}
	cout << endl << endl;
}

// XU LY 
//truong hop 1 y tang;
int yTang (TriedNode*root, int x, int y, int sizeY){
	char tuKhoa[30] = "";
	int z = 0, finalY = y;
	while(grid[x][y] - 'a' >= 0 && y < sizeY && root -> A[grid[x][y] - 'a'] != NULL){
		tuKhoa[z++] = grid[x][y];
		root = root -> A[grid[x][y] - 'a'];
		if(root -> endOfWord) luuKetQua(x, finalY, tuKhoa);
		y++;
	}
}
//truong hop 2 x tang;
int xTang (TriedNode*root, int x, int y, int sizeX){
	char tuKhoa[30] = "";
	int z = 0, finalX = x;
	while(grid[x][y] - 'a' >= 0 && x < sizeX && root -> A[grid[x][y] - 'a'] != NULL){
		tuKhoa[z++] = grid[x][y];
		root = root -> A[grid[x][y] - 'a'];
		if(root -> endOfWord) luuKetQua(finalX, y, tuKhoa);
		x++;
	}
}
//truong hop 3 x giam;
int xGiam (TriedNode*root, int x, int y, int sizeX){
	char tuKhoa[30] = "";
	int z = 0, finalX = x;
	TriedNode*tree = root;
	while(grid[x][y] - 'a' >= 0 && x >= 0 && root -> A[grid[x][y] - 'a'] != NULL){
		tuKhoa[z++] = grid[x][y];
		root = root -> A[grid[x][y] - 'a'];
		if(root -> endOfWord) luuKetQua(finalX, y, tuKhoa);
		x--;
	}
}
//truong hop 4 y giam
int yGiam (TriedNode*root, int x, int y, int sizeY){
	char tuKhoa[30] = "";
	int z = 0, finalY = y;
	while(grid[x][y] - 'a' >= 0 && y >= 0 && root -> A[grid[x][y] - 'a'] != NULL){
		tuKhoa[z++] = grid[x][y];
		root = root -> A[grid[x][y] - 'a'];
		if(root -> endOfWord) luuKetQua(x, finalY, tuKhoa);
		y--;
	}
}
//truong hop 5 x giam y tang
int xGiamYTang(TriedNode*root, int x, int y, int sizeY){
	char tuKhoa[30] = "";
	int z = 0, finalX = x, finalY = y;
	while(grid[x][y] - 'a' >= 0 && x >= 0 && y < sizeY && root -> A[grid[x][y] - 'a'] != NULL){
		tuKhoa[z++] = grid[x][y];
		root = root -> A[grid[x][y] - 'a'];
		if(root -> endOfWord) luuKetQua(finalX, finalY, tuKhoa);
		x--, y++;
	}
}
// truong hop 6 x tang y giam
int xTangYGiam(TriedNode*root, int x, int y, int sizeX){
	char tuKhoa[30] = "";
	int z = 0, finalX = x, finalY = y;
	TriedNode*tree = root;
	while(grid[x][y] - 'a' >= 0 && x < sizeX && y >= 0 && root -> A[grid[x][y] - 'a'] != NULL){
		tuKhoa[z++] = grid[x][y];
		root = root -> A[grid[x][y] - 'a'];
		if(root -> endOfWord) luuKetQua(finalX, finalY, tuKhoa);
		x++; y--;
	}
}
// truong hop 7 x giam y giam
int xGiamYGiam(TriedNode*root, int x, int y){
	char tuKhoa[30] = "";
	int z = 0, finalX = x, finalY = y;
	TriedNode*tree = root;
	while(grid[x][y] - 'a' >= 0 && x >= 0 && y >= 0 && root -> A[grid[x][y] - 'a'] != NULL){
		tuKhoa[z++] = grid[x][y];
		root = root -> A[grid[x][y] - 'a'];
		if(root -> endOfWord) luuKetQua(finalX, finalY, tuKhoa);
		x--; y--;
	}
}
// truong hop 8 x tang y tang
int xTangYTang(TriedNode*root, int x, int y, int sizeX, int sizeY){
	char tuKhoa[30] = "";
	int z = 0, finalX = x, finalY = y;
	while(grid[x][y] - 'a' >= 0 && x < sizeX && y < sizeY && root -> A[grid[x][y] - 'a'] != NULL){
		tuKhoa[z++] = grid[x][y];
		root = root -> A[grid[x][y] - 'a'];
		if(root -> endOfWord) luuKetQua(finalX, finalY, tuKhoa);
		x++; y++;
	}
}
int xuLy(TriedNode*root, int sizeX, int sizeY){
	for(int i = 0;i < sizeX; i++){
		for(int j = 0;j < sizeY; j++){
			yTang(root, i, j, sizeY);
			xTang(root, i, j, sizeX);
			xGiam(root, i, j, sizeX);
			yGiam(root, i, j, sizeY);
			xGiamYTang(root, i, j, sizeY);
			xTangYGiam(root, i, j, sizeX);
			xGiamYGiam(root, i, j);
			xTangYTang(root, i, j, sizeX, sizeY);
		}
	}
}

// XU LY DE LUU CAC GIA TRI VA IN RA KET QUA
char*chuyen(int so){
	if(so == 0) return "0";
	char*traChuoi = (char*)malloc(sizeof(char) * 4);
	char chuoi[4] = "";
	int z = 0;
	while(so > 0){
		chuoi[z++] = (char)(so % 10 + 48);
		so /= 10;
	}
	strrev(chuoi);
	strcpy(traChuoi, chuoi);
	return traChuoi;
}

void luuKetQua(int x, int y, char word[]){
	char viTri[20] = "";
	    strcat(viTri, "(");
		strcat(viTri, chuyen(x));
		strcat(viTri, ",");
		strcat(viTri, chuyen(y));
		strcat(viTri, ") ");
	for(int i = 0;i < tongSoTu; i++){
		if(strcmp(array[i].Tu, word) == 0){
			int t = strlen(array[i].viTri);
			if(t % 100 <= 99 && t % 100 >= 92)
			array[i].viTri[strlen(array[i].viTri)] = '\n';
			strcat(array[i].viTri, viTri);
			return;
		}
	}
	strcpy(array[tongSoTu].Tu, word);
	strcat(array[tongSoTu].viTri, viTri);
	tongSoTu++;
}

void inKetQua(){
	cout << "Tong so tu tim duoc la : " << tongSoTu << endl << endl;
	for(int i = 0;i < tongSoTu; i++){
		printf("%d. %s \n", i + 1, array[i].Tu);
		printf("%s\n\n", array[i].viTri);
	}
}
void chayChuongTrinh(TriedNode* root){
	int sizeX = 0, sizeY = 0;
	docVaInFileGrid(sizeX, sizeY);
	xuLy(root, sizeX, sizeY);
	inKetQua();
}
int main(){
	cout << "*************Chuong trinh tim tu tren Gird****************"<< endl;
	cout << "__________________________________________________________"<< endl;
	TriedNode*root = init();
	layDuLieu(root);
	chayChuongTrinh(root);
	char kt;
	cout << "Ban muon chay tiep: Y or N ?   ";
	cin >> kt;
	while(kt=='Y')
	{
		system("cls");
		chayChuongTrinh(root);
		cout << "Ban muon chay tiep: Y or N ?   ";
		cin >> kt;
	} 	
	system("pause");
}
